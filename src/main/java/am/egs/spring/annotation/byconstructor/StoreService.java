package am.egs.spring.annotation.byconstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by haykh on 4/29/2019.
 */
@Component
public class StoreService {

  DatabaseConnection dbConnections;

  // Constructor based DI
  @Autowired
  public StoreService(DatabaseConnection dbConnections){
    this.dbConnections = dbConnections;
  }

  public String showDBUrl(){
    return dbConnections.getUrl();
  }
}
